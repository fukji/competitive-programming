#include <bits/stdc++.h>
using namespace std;

int main(){

	int n, m, k, moves;
	string res = "";
	cin >> n >> m >> k;
	moves = n + m - 2;
	for(int i = 0; i < n-1; i++){
		res += 'U';
	}
	for(int i = 0; i < m-1; i++){
		res += 'L';
	}
	for(int i = 0; i < n; i++){
		if(i > 0){
			res += 'D';
			moves++;
		}
		for(int j = 0; j < m-1; j++){
			if(i & 1){
				res += 'L';
			}
			else{
				res += 'R';
			}
			moves++;
		}
	}
	cout << moves << '\n' << res << '\n';
	return 0;
}