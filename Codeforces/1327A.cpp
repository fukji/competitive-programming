#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

int main(){

	ll tc, n, k;
	cin >> tc;
	while(tc--){
		cin >> n >> k;
		n -= k*k;
		if(n >= 0 && n % 2 == 0){
			cout << "YES\n";
		}
		else{
			cout << "NO\n";
		}
	}
	return 0;
}