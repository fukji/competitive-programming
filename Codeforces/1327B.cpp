#include <bits/stdc++.h>
using namespace std;

int taken[100005];

int main(){

	int tc, n, k, g;
	int take, cnt, single;
	cin >> tc;
	while(tc--){
		cin >> n;
		for(int i = 0; i < n; i++){
			taken[i] = 0;
		}
		cnt = 0;
		for(int i = 0; i < n; i++){
			cin >> k;
			take = -1;
			for(int j = 0; j < k; j++){
				cin >> g;
				if(take == -1 && taken[g-1] == 0){
					take = g-1;
				}
			}
			if(take != -1){
				taken[take] = 1;
				cnt++;
			}
			else{
				single = i+1;
			}
		}
		if(cnt == n){
			cout << "OPTIMAL\n";
		}
		else{
			cout << "IMPROVE\n";
			cout << single << " ";
			for(int i = 0; i < n; i++){
				if(taken[i] == 0){
					cout << i+1 << '\n';
					break;
				}
			}
		}
	}
	return 0;
}